import React, {Component} from 'react'
import ChatList from './ChatList'
import Horizon from '@horizon/client'
const horizon = Horizon({ secure: false })
const chat = horizon('messages')
import styled from 'styled-components'

export default class Chat extends Component {

  constructor(...props) {
    super(...props)

    this.sendChat = this.sendChat.bind(this)
    this.clearFields = this.clearFields.bind(this)
    this.clearChat = this.clearChat.bind(this)
  }

  componentDidMount() {
    this.submitOnEnter()
  }

  sendChat(event) {
    event.preventDefault()

    const message = {
      name: this.name.value,
      text: this.text.value,
      date: new Date()
    }
    if (this.name.value && this.text.value)
      chat.store(message)
    this.clearFields()
    this.name.focus()
  }

  clearFields() {
    const fields = [
      this.name,
      this.text
    ]
    fields.forEach(field => {field.value = ''})
  }

  submitOnEnter() {
    document.addEventListener("keypress", (event) => {
      if (event.keyCode === 13)
        this.sendChat(event)
    })
  }

  clearChat() {
    chat.fetch().subscribe(messages => {
      chat.removeAll(messages)
    }, (err) => {
      console.error(err)
    })
  }

  render () {
    return (
      <Wrapper>
        <Form onSubmit={this.sendChat}>
          <Title>Do chat</Title>
          <InputText
            type="text"
            placeholder="Your a.k.a"
            innerRef={(comp) => { this.name = comp }}
          />
          <div>
            <Textarea
              placeholder="Your message"
              innerRef={(comp) => { this.text = comp }}
          />
          </div>
          <Button
            type="submit"
          >
            Send
          </Button>
          <Button
            onClick={this.clearChat}
            clean
          >
            Clear chat
          </Button>
        </Form>
        <ChatList
          messages={chat}
        />
      </Wrapper>
    )
  }
}

const commonStyles = `
  display: flex
  border: 1px solid #dedede
  border-radius: 2px
  width: 100%
  margin: 20px 0
  font-size: 14px
  box-sizing: border-box
`

const Wrapper = styled.div`
  display: flex
  flex-direction: row-reverse
`

const Form = styled.form`
  flex: 1
  margin-left: 20px;
`

const InputText = styled.input`
  ${commonStyles}
  height: 40px;
  padding: 0 10px
`
const Textarea = styled.textarea`
  ${commonStyles}
  height: 250px
  resize: none
  padding: 10px
`
const Title = styled.h2`
  font-size: 30px;
  text-transform: uppercase;
  letter-spacing: 2px;
`

const Button = styled.button`
  font-size: 16px;
  margin: 20px 5px 0 0;
  padding: 10px 35px;
  border: 3px solid;
  border-color: ${props => {return props.clean ? '#323232' : 'palevioletred'}}
  background-color: white;
  border-radius: 2px;
  cursor: pointer;
`
