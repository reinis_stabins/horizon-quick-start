import React, {Component, PropTypes} from 'react'
import styled from 'styled-components'

export default class ChatList extends Component {

  constructor(...props) {
    super(...props)

    this.state = {
      messages: []
    }
    this.generateChatMessages = this.generateChatMessages.bind(this)
  }

  static propTypes = {
    messages: PropTypes.object
  }

  componentDidMount() {
    this.props.messages.order('date', 'descending').watch().subscribe(messages => {
      this.setState({messages: messages})
    }, (err) => {
      console.error(err)
    })
  }

  generateChatMessages() {
    return this.state.messages.map((item, key) => (
      <ListItem key={key}>
        <Delete
          onClick={() => this.deleteItem(item.id)}
        >
          Delete
        </Delete>
        <Title>@{item.name}</Title>
        <Comment>{item.text}</Comment>
      </ListItem>
    ))
  }

  deleteItem(id) {
    this.props.messages.remove(id);
  }

  render () {
    return (
      <Wrapper>
        <List>{this.generateChatMessages()}</List>
      </Wrapper>
    )
  }
}

const Wrapper = styled.div`
  flex: 1
`

const List = styled.ul`
  list-style-type: none;
  padding: 0;
`

const ListItem = styled.li`
  margin: 0 0 36px;
  padding: 0 0 24px;
  position: relative;
`

const Title = styled.h3`
  margin: 0
`

const Comment = styled.p`
  color: palevioletred
  margin: 10px 0 0
`

const Delete = styled.div`
  font-size: 14px;
  position: absolute;
  bottom: 0;
  left: 0;
  cursor: pointer;
`
