import React, {Component} from 'react'
import Chat from './Chat/Chat'
import styled from 'styled-components'

export default class App extends Component {
  render () {
    return (
      <Wrapper>
        <Chat />
      </Wrapper>
    )
  }
}

const Wrapper = styled.div`
  max-width: 760px;
  margin: 0 auto;
  color: #323232;
  font-family: sans-serif;
`
